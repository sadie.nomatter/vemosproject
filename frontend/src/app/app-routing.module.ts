import { WhatsUnipalComponent } from './components/login/whats-unipal/whats-unipal.component';
import { BlockGuardService } from './services/block-guard.service';
import { MyCoursesComponent } from './components/my-courses/my-courses.component';
import { CourseResolver } from './resolvers/course.resolver';
import { DiscussionResolver } from './resolvers/discussion.resolver';
import { CreateDiscussionComponent } from './components/discussion/create-discussion/create-discussion.component';
import { DiscussionDetailComponent } from './components/discussion/discussion-detail/discussion-detail.component';
import { NgModule } from '@angular/core';
import { HomepageComponent } from './components/homepage/homepage.component';
import { Routes, RouterModule, CanDeactivate } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SearchListComponent } from './components/search-list/search-list.component';
import { SelectedCourseComponent } from './components/course/selected-course/selected-course.component';
import { JoinedCourseComponent } from './components/course/joined-course/joined-course.component';
import { OtherUserProfileComponent } from './components/other-user-profile/other-user-profile.component';
import { PersonalProfileComponent } from './components/personal-profile/personal-profile.component';
import { PalsTutorsComponent } from './components/pals-tutors/pals-tutors.component';
import { AuthGuardService } from './services/auth-guard.service';
import { UserResolver } from './resolvers/user.resolver';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'homepage' },
      { path: 'results', component: SearchListComponent },
      { path: 'homepage', component: HomepageComponent },
      {
        path: 'course/:id',
        component: SelectedCourseComponent,
        resolve: { resData: CourseResolver }
      },
      { path: 'joined-course', component: JoinedCourseComponent },
      {
        path: 'user-profile/:id',
        component: OtherUserProfileComponent,
        resolve: { resData: UserResolver }
      },
      { path: 'createDiscussion', component: CreateDiscussionComponent },
      {
        path: 'discussion/:id',
        component: DiscussionDetailComponent,
        resolve: { resData: DiscussionResolver }
      },
      { path: 'personal-profile', component: PersonalProfileComponent },
      { path: 'pals-tutors', component: PalsTutorsComponent },
      { path: 'myCourses', component: MyCoursesComponent }
    ]
  },
  {
    path: '',
    canActivate: [BlockGuardService],
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'login' },
      { path: 'login', component: LoginComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      { path: 'whats-unipal', component: WhatsUnipalComponent }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
