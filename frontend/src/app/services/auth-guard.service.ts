import { Injectable, Component } from '@angular/core';
import { CanActivate, CanDeactivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private authService: AuthService, private route: Router) {}

  canActivate() {
    if (this.authService.isAuthenticated()) {
      return true;
    }
    this.route.navigate(['/login']);
    return false;
  }
}
