import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable, Component } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BlockGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    if (this.authService) {
      if (this.authService.isAuthenticated()) {
        this.router.navigate(['/homepage']);
      }
      return true;
    }
    return false;
  }
}
