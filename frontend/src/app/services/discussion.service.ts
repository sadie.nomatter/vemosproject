import { environment } from 'src/environments/environment';
import { Response } from './../shared/response';
import { Observable } from 'rxjs';
import { Discussion } from './../shared/discussion';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const DISC = '/discussion';
const USER = '/user';
const COURSE = '/course';

@Injectable({
  providedIn: 'root'
})
export class DiscussionService {
  constructor(private http: HttpClient) {}

  getDiscussion(id: string): Observable<Discussion> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Discussion>(`${environment.APIEndpoint}${DISC}/${id}`, {
      headers
    });
  }

  createDiscussion(discussion: Discussion) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(`${environment.APIEndpoint}${DISC}`, discussion, { headers });
  }

  editQuestion(id: string, questionText: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${DISC}/${id}`,
      { questionText },
      {
        headers
      }
    );
  }

  deleteDiscussion(id: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.delete(`${environment.APIEndpoint}${DISC}/${id}`, { headers });
  }

  likeDiscussion(id: string, userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${DISC}/like/${id}`,
      { userId },
      {
        headers
      }
    );
  }

  createComment(newComment: { discussionId: string; commentText: string; commentCreator: Object }) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(`${environment.APIEndpoint}${DISC}/comment`, newComment, { headers });
  }

  editComment(discussionId: string, commentId: string, commentText: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${DISC}/editComment/${discussionId}`,
      { commentId, commentText },
      {
        headers
      }
    );
  }

  deleteComment(discussionId: string, commentId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${DISC}/deleteComment/${discussionId}`,
      { commentId },
      {
        headers
      }
    );
  }

  likeComment(discussionId: string, commentId: string, userId: string) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(
      `${environment.APIEndpoint}${DISC}/likeComment/${discussionId}`,
      { commentId, userId },
      {
        headers
      }
    );
  }

  getDiscussionsByCourse(courseId: string): Observable<Response> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Response>(`${environment.APIEndpoint}${DISC}${COURSE}/${courseId}`, {
      headers
    });
  }

  getDiscussionsByUser(creatorId: string): Observable<Discussion[]> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Discussion[]>(`${environment.APIEndpoint}${DISC}${USER}/${creatorId}`, {
      headers
    });
  }

  getDiscussionsByUserAndCourse(creatorId: string, courseId: string): Observable<Discussion[]> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get<Discussion[]>(
      `${environment.APIEndpoint}${DISC}${USER}/${creatorId}${COURSE}/${courseId}`,
      {
        headers
      }
    );
  }
}
