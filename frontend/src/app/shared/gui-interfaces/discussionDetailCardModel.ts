import { User } from './../user';
export interface DiscussionDetailCardObject {
  id?: string;
  creator: User;
  title?: string;
  subtitle?: string;
  mainText: string;
  likes?: number;
  commentsTotal?: number;
  date?: Date;
  likedBy: string[];
  parentId?: string;
  course: string;
}
