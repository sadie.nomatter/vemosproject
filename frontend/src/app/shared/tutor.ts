import { University } from './university';

export interface Tutor {
  name: string;
  profilePicture: string;
  university?: University;
  starRating: number;
  fee: number;
}
