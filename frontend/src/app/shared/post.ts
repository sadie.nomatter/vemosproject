import { User } from './user';

export interface Discussion {
  title: string;
  user: User;
  creationDate: Date;
}
