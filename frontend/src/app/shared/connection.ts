import { Course } from './course';
export interface Connection {
  name: string;
  course: Course;
  connectionDate: Date;
  profilePicture?: string;
}
