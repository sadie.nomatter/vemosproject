import { User } from './user';

export interface Notification {
  _id?: string;
  toUser?: User | string;
  fromUser: User | string;
  requestDate?: Date;
  resonseDate?: Date;
  isRead?: boolean;
  status?: string;
  course?: { _id: string; name: string };
  requestType: string;
  discussionId?: string;
}
