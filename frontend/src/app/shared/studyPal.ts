import { University } from './university';

export interface StudyPal {
  name: string;
  profilePicture: string;
  university?: University;
  starRating: number;
}
