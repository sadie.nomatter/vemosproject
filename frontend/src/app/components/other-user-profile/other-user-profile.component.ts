import { NotificationService } from 'src/app/services/notification.service';
import { CourseService } from 'src/app/services/course.service';
import { Response } from './../../shared/response';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/user';
import { Course } from 'src/app/shared/course';

@Component({
  selector: 'app-other-user-profile',
  templateUrl: './other-user-profile.component.html',
  styleUrls: ['./other-user-profile.component.css']
})
export class OtherUserProfileComponent implements OnInit {
  user: User;
  palCourses: Course[];
  tutorCourses: Course[];
  myPals: Notification[];
  myTutors: Notification[];

  constructor(
    private courseService: CourseService,
    private notificationService: NotificationService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const response: Response = this.route.snapshot.data.resData;
    if (response.success) {
      this.user = response.object;
      this.courseService.getMyCoursesPal(this.user._id).subscribe((res) => {
        if (res.success) {
          this.palCourses = res.object;
        }
      });
      this.courseService.getMyCoursesTutor(this.user._id).subscribe((res) => {
        if (res.success) {
          this.tutorCourses = res.object;
        }
      });
      this.loadMyPals();
      this.loadMyTutors();
    }
  }

  getPalRating() {
    return (
      this.user.palRatings.reduce((accumulator, current) => accumulator + current.rating, 0) /
      this.user.palRatings.length
    );
  }

  getTutorRating() {
    return (
      this.user.tutorRatings.reduce((accumulator, current) => accumulator + current.rating, 0) /
      this.user.tutorRatings.length
    );
  }

  loadMyPals() {
    this.notificationService
      .getMyPals(this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.myPals = response.object;
        }
      });
  }

  loadMyTutors() {
    this.notificationService
      .getMyTutors(this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.myTutors = response.object;
        }
      });
  }
}
