import { UserService } from 'src/app/services/user.service';
import { Notification } from 'src/app/shared/notification';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-request-card',
  templateUrl: './request-card.component.html',
  styleUrls: ['./request-card.component.css']
})
export class RequestCardComponent {
  @Input() request: Notification;
  @Input() isIncomingRequest: boolean = false;
  @Output() reloadComponent = new EventEmitter<boolean>();

  constructor(
    private notificationService: NotificationService,
    private userService: UserService,
    private snackbar: MatSnackBar
  ) {}

  accept() {
    this.notificationService.answerNotification(this.request._id, 'accepted').subscribe((data) => {
      this.reloadComponent.emit(true);
      this.notificationService.updateNotifications(true);
      this.snackbar.open('Notification successfully accepted', 'Remove', {
        duration: 5000
      });
    });
    const newNotification: Notification = {
      toUser: this.isIncomingRequest ? this.request.fromUser['_id'] : this.request.toUser['_id'],
      fromUser: this.userService.getUser()._id,
      status: 'accepted',
      requestType: 'response'
    };
    this.notificationService.createNotification(newNotification).subscribe(() => {});
  }

  reject() {
    this.notificationService.answerNotification(this.request._id, 'rejected').subscribe((data) => {
      this.reloadComponent.emit(true);
      this.notificationService.updateNotifications(true);
      this.snackbar.open('Notification successfully rejected', 'Remove', {
        duration: 5000
      });
    });
    const newNotification: Notification = {
      toUser: this.isIncomingRequest ? this.request.fromUser['_id'] : this.request.toUser['_id'],
      fromUser: this.userService.getUser()._id,
      status: 'rejected',
      requestType: 'response'
    };
    this.notificationService.createNotification(newNotification).subscribe(() => {});
  }
}
