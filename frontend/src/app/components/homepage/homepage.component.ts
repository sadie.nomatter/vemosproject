import { Course } from 'src/app/shared/course';
import { CourseService } from 'src/app/services/course.service';
import { User } from './../../shared/user';
import { NotificationService } from './../../services/notification.service';
import { UserService } from './../../services/user.service';
import { DiscussionService } from './../../services/discussion.service';
import { Discussion } from '../../shared/discussion';
import { Response } from './../../shared/response';
import { Connection } from '../../shared/connection';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  discussions: Discussion[];
  connections: Connection[];
  joinedCoursesMember: Course[];

  constructor(
    private userService: UserService,
    private discussionService: DiscussionService,
    private notificationService: NotificationService,
    private coursesService: CourseService
  ) {}

  ngOnInit() {
    this.discussionService
      .getDiscussionsByUser(this.userService.getUser()._id)
      .subscribe((data: Discussion[]) => {
        if (data) {
          this.discussions = data;
        }
      });

    this.notificationService
      .getConnections(this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.connections = response.object;
        }
      });
    this.coursesService
      .getRecentlyJoinedCourses(this.userService.getUser()._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.joinedCoursesMember = response.object;
        }
      });
  }

  isCurrentUser(fromUser: User) {
    return this.userService.getUser()._id == fromUser?._id;
  }

  getJoinedDate(course: Course) {
    return course.members.find((m) => m._id === this.userService.getUser()._id).joinDate;
  }
}
