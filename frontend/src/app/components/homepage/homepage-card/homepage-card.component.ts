import { Component, Input, OnInit } from '@angular/core';
import { HomepageCardModel } from 'src/app/shared/gui-interfaces/homepageCardModel';

@Component({
  selector: 'app-homepage-card',
  templateUrl: './homepage-card.component.html',
  styleUrls: ['./homepage-card.component.css']
})
export class HomepageCardComponent {
  @Input() cardObject: HomepageCardModel;

  constructor() {}
}
