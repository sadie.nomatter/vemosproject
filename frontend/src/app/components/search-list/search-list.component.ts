import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { University } from '../../shared/university';
import { CourseService } from '../../services/course.service';
import { Course } from '../../shared/course';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {
  courses: Course[];
  uniList: University[];
  private savedCourses: Course[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private courseService: CourseService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getAllUniversities();
    this.loadCourses();
  }

  sort(value: string) {
    if (value === 'sortAZ') {
      return this.courses.sort((a, b) => a.name.toLowerCase().localeCompare(b.name));
    } else {
      return this.courses.sort((a, b) => b.name.toLowerCase().localeCompare(a.name));
    }
  }

  filterByUni(uni: University) {
    if (this.savedCourses) {
      this.courses = this.savedCourses.map((x) => Object.assign({}, x));
    }
    this.savedCourses = this.courses.map((x) => Object.assign({}, x));
    this.courses = this.courses.filter((course) => course.university.name === uni.name);
  }

  loadCourses() {
    this.activatedRoute.queryParams.subscribe(async (param) => {
      let courseQuery = param.search_query;
      this.courses = await this.courseService
        .getCourses(courseQuery)
        .then((res) => res)
        .catch((error) => {
          this.snackBar.open(error, 'Remove', {
            duration: 5000
          });
          return [];
        });
    });
  }

  async getAllUniversities() {
    this.uniList = await this.courseService.getUniList().then((uniList) => uniList);
  }

  openCourse(id: string) {
    this.courseService;
  }
}
