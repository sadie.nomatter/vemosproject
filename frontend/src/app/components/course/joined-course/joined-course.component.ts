import { UserService } from './../../../services/user.service';
import { NotificationService } from './../../../services/notification.service';
import { Response } from './../../../shared/response';
import { Course } from 'src/app/shared/course';
import { Discussion } from '../../../shared/discussion';
import { DiscussionService } from '../../../services/discussion.service';
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CourseService } from 'src/app/services/course.service';
import { University } from 'src/app/shared/university';
import { Notification } from 'src/app/shared/notification';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-joined-course',
  templateUrl: './joined-course.component.html',
  styleUrls: ['./joined-course.component.css']
})
export class JoinedCourseComponent implements OnInit {
  @Input() course: Course;
  discussions: Discussion[];
  uniList: University[];
  post = '';
  pals: Object[];
  tutors: Object[];
  myPals: Notification[];
  myTutors: Notification[];

  currentPageDiscussions: number = 0;
  pageSizeDiscussions: number = 10;
  discussionsPaginated: Object[];

  currentPagePals: number = 0;
  pageSizePals: number = 12;
  palsPaginated: Object[];

  currentPageTutors: number = 0;
  pageSizeTutors: number = 12;
  tutorsPaginated: Object[];

  constructor(
    private courseService: CourseService,
    private discussionService: DiscussionService,
    private notificationService: NotificationService,
    private userService: UserService,
    private router: Router
  ) {}

  @HostListener('keydown.enter', ['$event']) onEnterKeyDown(event: KeyboardEvent) {
    this.createDiscussion();
  }

  ngOnInit(): void {
    this.loadMyPals();
    this.loadMyTutors();

    this.discussionService.getDiscussionsByCourse(this.course._id).subscribe((data: Response) => {
      if (data.success) {
        this.discussions = data.object;
        this.updateDiscussionsArray();
      }
    });
    this.courseService.getCoursePals(this.course._id).subscribe((data: Response) => {
      if (data.success) {
        this.pals = data.object;
        this.updatePalsArray();
      }
    });
    this.courseService.getCourseTutors(this.course._id).subscribe((data: Response) => {
      if (data.success) {
        this.tutors = data.object;
        this.updateTutorsArray();
      }
    });

    this.loadUniList();
  }

  async loadUniList() {
    this.uniList = await this.courseService.getUniList().then((uniList) => uniList);
  }

  createDiscussion() {
    this.router.navigate(['createDiscussion', { title: this.post, courseId: this.course._id }]);
  }

  openDiscussion(id: string) {
    this.router.navigate(['discussion', id]);
  }

  loadMyPals() {
    this.notificationService
      .getMyPalsForCourse(this.userService.getUser()._id, this.course._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.myPals = response.object;
        }
      });
  }

  loadMyTutors() {
    this.notificationService
      .getMyTutorsForCourse(this.userService.getUser()._id, this.course._id)
      .subscribe((response: Response) => {
        if (response.success) {
          this.myTutors = response.object;
        }
      });
  }

  isLiked(discussion: Discussion) {
    return discussion.likedBy?.includes(this.userService.getUser()._id);
  }

  openUserProfile(userId: string) {
    this.router.navigate(['user-profile/', userId]);
  }

  paginatorButtonQuestions(pageEvent: PageEvent) {
    this.currentPageDiscussions = pageEvent.pageIndex;
    this.pageSizeDiscussions = pageEvent.pageSize;
    this.updateDiscussionsArray();
  }

  updateDiscussionsArray() {
    const start = this.currentPageDiscussions * this.pageSizeDiscussions;
    const end = (this.currentPageDiscussions + 1) * this.pageSizeDiscussions;
    this.discussionsPaginated = this.discussions.slice(start, end);
  }

  paginatorButtonPals(pageEvent: PageEvent) {
    this.currentPagePals = pageEvent.pageIndex;
    this.pageSizePals = pageEvent.pageSize;
    this.updatePalsArray();
  }

  updatePalsArray() {
    const start = this.currentPagePals * this.pageSizePals;
    const end = (this.currentPagePals + 1) * this.pageSizePals;
    this.palsPaginated = this.pals.slice(start, end);
  }

  paginatorButtonTutors(pageEvent: PageEvent) {
    this.currentPageTutors = pageEvent.pageIndex;
    this.pageSizeTutors = pageEvent.pageSize;
    this.updateDiscussionsArray();
  }

  updateTutorsArray() {
    const start = this.currentPageTutors * this.pageSizeTutors;
    const end = (this.currentPageTutors + 1) * this.pageSizeTutors;
    this.tutorsPaginated = this.tutors.slice(start, end);
  }
}
