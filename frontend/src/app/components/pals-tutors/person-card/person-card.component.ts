import { Response } from './../../../shared/response';
import { UserService } from './../../../services/user.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/shared/user';
import { Notification } from 'src/app/shared/notification';

@Component({
  selector: 'app-person-card',
  templateUrl: './person-card.component.html',
  styleUrls: ['./person-card.component.css']
})
export class PersonCardComponent {
  @Input() connection: Notification;
  @Input() isTutee: boolean = false;
  @Output() reloadPals: EventEmitter<Object> = new EventEmitter();
  @Output() reloadTutors: EventEmitter<Object> = new EventEmitter();

  constructor(private userService: UserService) {}

  isCurrentUser(fromUser: User) {
    return this.userService.getUser()._id == fromUser._id;
  }

  getOtherUser() {
    if (this.userService.getUser()._id == this.connection.fromUser['_id']) {
      return this.connection.toUser;
    } else {
      return this.connection.fromUser;
    }
  }

  getRating() {
    let user = this.getOtherUser();
    if (this.connection.requestType == 'pal') {
      return this.getPalRating(user);
    } else if (this.connection.requestType == 'tutor') {
      return this.getTutorRating(user);
    }
  }

  getPalRating(user) {
    return (
      user.palRatings.reduce((accumulator, current) => accumulator + current.rating, 0) /
      user.palRatings.length
    );
  }

  getTutorRating(user) {
    return (
      user.tutorRatings.reduce((accumulator, current) => accumulator + current.rating, 0) /
      user.tutorRatings.length
    );
  }

  rateUser(rating) {
    let user: any = this.getOtherUser();
    if (this.connection.requestType == 'pal') {
      return this.ratePal(user._id, rating);
    } else if (this.connection.requestType == 'tutor') {
      return this.rateTutor(user._id, rating);
    }
    rating.stopPropagation();
  }

  ratePal(palId, rating) {
    if (rating) {
      this.userService
        .ratePal(palId, this.userService.getUser()._id, rating)
        .subscribe((response: Response) => {
          if (response.success) {
            this.reloadPals.emit();
          }
        });
    }
  }

  rateTutor(tutorId, rating) {
    if (rating) {
      this.userService
        .rateTutor(tutorId, this.userService.getUser()._id, rating)
        .subscribe((response: Response) => {
          if (response.success) {
            this.reloadTutors.emit();
          }
        });
    }
  }
}
