import { ActivatedRoute } from '@angular/router';
import { Discussion } from '../../../shared/discussion';
import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-discussion-detail',
  templateUrl: './discussion-detail.component.html',
  styleUrls: ['./discussion-detail.component.css']
})
export class DiscussionDetailComponent implements OnInit {
  discussion: Discussion;
  currentPage: number = 0;
  pageSize: number = 10;
  commentsArray: Object[];

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.discussion = this.route.snapshot.data.resData;
    this.updateCommentsArray();
  }

  updateDiscussion(updatedDiscussion: Discussion) {
    this.discussion = updatedDiscussion;
    this.updateCommentsArray();
  }

  paginatorButton(pageEvent: PageEvent) {
    this.currentPage = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.updateCommentsArray();
  }

  updateCommentsArray() {
    const start = this.currentPage * this.pageSize;
    const end = (this.currentPage + 1) * this.pageSize;
    this.commentsArray = this.discussion.comments.slice(start, end);
  }
}
