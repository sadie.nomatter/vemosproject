import { Response } from './../../../shared/response';
import { UserService } from './../../../services/user.service';
import { Discussion } from './../../../shared/discussion';
import { DiscussionService } from './../../../services/discussion.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-discussion',
  templateUrl: './create-discussion.component.html',
  styleUrls: ['./create-discussion.component.css']
})
export class CreateDiscussionComponent implements OnInit {
  questionTitle: string;
  questionText: string;
  courseId: string;

  constructor(
    private discussionService: DiscussionService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const title = this.route.snapshot.paramMap.get('title');
    if (title) {
      this.questionTitle = title;
    }
    const courseId = this.route.snapshot.paramMap.get('courseId');
    if (courseId) {
      this.courseId = courseId;
    }
  }

  submitDiscussion() {
    const newDiscussion: Discussion = {
      title: this.questionTitle,
      questionText: this.questionText,
      creator: this.userService.user,
      course: this.courseId
    };
    this.discussionService.createDiscussion(newDiscussion).subscribe((data: Response) => {
      if (data.success) {
        this.router.navigate(['discussion', data.objectId]);
      } else {
        this.router.navigate(['homepage']);
      }
    });
  }

  cancel() {
    this.router.navigate(['homepage']);
  }
}
