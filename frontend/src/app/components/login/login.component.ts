import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  email: string;
  password: string;
  open: boolean = false;

  constructor(
    private userService: UserService,
    private snackbar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  login() {
    this.userService.login(this.email, this.password);
  }

  createAccount(account) {
    this.userService.signUp(account).subscribe((data) => {
      this.snackbar.open('User successfully created!', 'Remove', {
        duration: 5000
      });
    });

    this.open = !this.open;
  }

  isOpen() {
    this.open = !this.open;
  }

  private initForm(): void {
    this.formGroup = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.maxLength(15)]],
      lastName: ['', [Validators.required, Validators.maxLength(15)]],
      email: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.maxLength(10)]],
      password: ['', [Validators.required]]
    });
  }
}
