import { User } from './../shared/user';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {
  transform(user: User, option: string): unknown {
    if (!user) {
      return '';
    }
    if (option === 'displayName') {
      return user.firstName + ' ' + user.lastName + ' (@' + user.username + ')';
    } else if (option === 'userName') {
      return '@' + user.username;
    } else if (option === 'tag') {
      return '(@' + user.username + ')';
    } else {
      return user.firstName + ' ' + user.lastName;
    }
  }
}
