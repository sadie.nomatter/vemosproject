# UNIPAL

Unipal is a place for you to find pals to study alongside with, offering each other mutual support, discussing different subjects, sharing knowledge and giving each other another level of insight into course readings and materials. Additionally, you can easily find tutors for each course or become one yourself.

![Intro](./frontend/images/intro.png)

## Deep dive

Unipal offers you the possibility to join multiple courses from different universities. In each course you will be able to interact with other pals/tutors by creating discussions, commenting on already existing discussions. Within each course, you will be able to see all pals/tutors who have joined that course and request/offer help to each one of them. After study sessions with pals/tutors, you will be able to rate them from 1 to 5 stars.

![Course discussions](./frontend/images/advWeb-1.png)

![Course pals](./frontend/images/advWeb-2.png)

Unipal provides a very user friendly UI. Via the side-bars you will be able to easily navigate to your homepage, joined courses, connections or personal profile. Furthermore, the notification system will inform you in realtime about different events, such as pals/tutors accepting invitation to study together, or new comments in an active discussion thread.

In the personal-profile page you will be able to edit your profile data. Additionally, you can offer your help either as a Pal or as a Tutor for each joined course. If you decide to offer help as tutor, you will be able to set the hourly payment rate. Incoming and submitted requests can also be seen in the personal-profile page.

![personal-profile](./frontend/images/personal-profile.png)

Unipal offers many more features which can be further seen in the demo below.

## Used technologies and architecture

In the following, the used technologies and libraries can be viewed within the technical architecture:

![technology-architecture](./frontend/images/technologies-architecture.png)


## Unipal advertisement

[![Unipal advertisement](https://img.youtube.com/vi/2s6aqiNpMVg/1.jpg)](https://www.youtube.com/watch?v=TJ2HxOT6tSk "Unipal advertisement")

### Local installation

To run the project locally, you will have to run both the backend and frontend. Each one has a separate guide located in the appropriate folder.

## Help

For any bugs or issues, contact us via info@vemos.com

## Authors

**Contributer names:**


Valentin Panagiev

Enio Sultan

Martin Vlahov

Saadet Güzelyurt
