# Backend

This project was generated with Node version 14.13.1.

## Prerequisite

1. Make sure you have node installed.
2. Run npm install in the backend folder.

## Development server

Run `node server.js` to run the dev server.

## API

The following endpoints are currently available:

### User related endpoints

#### POST /api/user/register

Used to register a user.

#### POST /api/user/authenticate

For authenticaiton when logging in.

#### PUT /api/user/ratePal/:id

When rating a pal by his ID.

#### PUT /api/user/rateTutor/:id

Rating a tutor by his ID.

#### PUT /api/user/updateUser/:id

Upadint personal-profile data.

#### GET /api/user/checkUserByEmail

Check if user exists.

#### POST /api/user/resetPassword/:id

Reset own password.

#### GET /api/user/:id

Find user by ID.

### Course related endpoints

#### GET /api/courses/?search_query=XXX

Find a course via name.

#### GET /api/courses/:id

Find a course via ID.

#### PUT /api/courses/joinMember/:id

Join course as member.

#### PUT /api/courses/leaveMember/:id

Leave course.

#### PUT /api/courses/joinPal/:id

Join course as Pal.

#### PUT /api/courses/leavePal/:id

Leave course as Pal.

#### PUT /api/courses/joinTutor/:id

Join course as tutor.

#### PUT /api/courses/leaveTutor/:id

Leave course as tutor.

#### GET /api/courses/myCourses/:id

Get all my courses.

#### GET /api/courses/myCoursesMember/:id

Get all my courses in which i'm a member.

#### GET /api/courses/myCoursesPals/:id

Get all my courses in which i'm a pal.

#### GET /api/courses/myCoursesTutors/:id

Get all my courses in which i'm a tutor.

#### GET /api/courses/recentlyJoined/:id

Get recently joined courses.

#### GET /api/courses/:id/pals

Get all pals in a course.

#### GET /api/courses/:id/tutors

Get all tutors in a course.

### Discussion related endpoints

#### GET /api/discussion/:id

Get discussion by id.

#### PUT /api/discussion/:id

Update discussion.

#### DELETE /api/discussion/:id

Remove discussion

#### PUT /api/discussion/like/:id

Like/unlike discussion.

#### POST /api/discussion/comment

Post a comment in a discussion.

#### PUT /api/discussion/editComment/:id

Edit comment in a discussion.

#### PUT /api/discussion/deleteComment/:id

Delete comment in a discussion.

#### PUT /api/discussion/likeComment/:id

Like a comment.

#### GET /api/discussion/user/:id

Get all discussions posted by a user.

#### GET /api/discussion/course/:id

Get all discussions of a course.

#### GET /api/discussion/user/:userId/course/:courseId

Get all discussions posted by a user for a specific course.

### Notification related endpoints

#### POST /api/notification

Post a new notification.

#### PUT /api/notification/:id

Update existing notification.

#### GET /api/notification/:toUser

Get incoming notifications to a user.

#### GET /api/notification/from/:fromUser

Get incoming notifications from a specific user.

#### GET /api/notification/to/:toUser

Get outgoing notifications to a specific user.

#### GET /api/notification/connections/:id

Get connections.

#### GET /api/notification/myPals/:id

Get all pals a user has connected with.

#### GET /api/notification/myTutors/:id

Get all tutors a user has connected with.

#### GET /api/notification/myTutees/:id

Get all tutess a user teaches as a tutor.

#### GET /api/notification/myPals/:id/course/:courseId

Get all pals a user has connected with in a specific course.

#### GET /api/notification/myTutors/:id/course/:courseId

Get all tutors a user has connected with in a specific course.

### University related endpoints

#### GET /api/universities/all

Get a list of all universities.
