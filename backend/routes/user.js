const express = require("express");
const router = express.Router();
const passport = require("passport");
require("../passport")(passport);
const jwt = require("jsonwebtoken");
const config = require("../config");
const User = require("../models/user");

// Register
router.post("/register", function (req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({ success: false, msg: "Please pass username and password." });
  } else {
    let newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
    });
    newUser.save(function (err) {
      if (err) {
        return res.json({ success: false, msg: "Username already exists." });
      }
      res.json({ success: true, msg: "Successful created new user." });
    });
  }
});

// Authenticate User
router.post("/authenticate", function (req, res) {
  User.findOne(
    {
      $or: [{ email: req.body.email }],
    },
    "+password",
    function (err, user) {
      if (err) console.log("err", err);

      if (!user) {
        res.status(401).send({
          success: false,
          msg: "Authentication failed. User not found.",
        });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            let token = jwt.sign(user.toJSON(), config.secret);
            // return the information including token as JSON
            let resUser = {
              _id: user._id,
              username: user.username,
              firstName: user.firstName,
              lastName: user.lastName,
              email: user.email,
            };
            res.json({ success: true, token: "JWT " + token, object: resUser });
          } else {
            res.status(401).send({
              success: false,
              msg: "Authentication failed. Wrong password.",
            });
          }
        });
      }
    }
  );
});

router.put(
  "/ratePal/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    User.findById(req.params.id, (err, user) => {
      if (err) {
        res.json({ success: false, message: "Failed to rate tutor" });
      } else {
        if (user.palRatings.id(req.body.ratingUserId)) {
          user.palRatings.id(req.body.ratingUserId).rating = req.body.rating;
        } else {
          user.palRatings.push({
            _id: req.body.ratingUserId,
            rating: req.body.rating,
          });
        }
        user.save((err, params) => {
          if (err) {
            console.log(err);
            res.json({ success: false, message: "Failed to rate pal" });
          } else {
            res.json({ success: true, message: "Successfully rated pal" });
          }
        });
      }
    });
  }
);

router.put(
  "/rateTutor/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    User.findById(req.params.id, (err, user) => {
      if (err) {
        res.json({ success: false, message: "Failed to rate tutor" });
      } else {
        if (user.tutorRatings.id(req.body.ratingUserId)) {
          user.tutorRatings.id(req.body.ratingUserId).rating = req.body.rating;
        } else {
          user.tutorRatings.push({
            _id: req.body.ratingUserId,
            rating: req.body.rating,
          });
        }
        user.save((err, params) => {
          if (err) {
            console.log(err);
            res.json({ success: false, message: "Failed to rate tutor" });
          } else {
            res.json({ success: true, message: "Successfully rated tutor" });
          }
        });
      }
    });
  }
);

router.put(
  "/updateUser/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    User.findByIdAndUpdate(
      req.params.id,
      {
        $set: {
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          username: req.body.username,
          description: req.body.description,
        },
      },
      { new: true },
      (err, user) => {
        if (err) {
          res.json({ success: false, message: "Failed to update user" });
        } else {
          res.json({
            success: true,
            message: "Successfully updated user",
            object: user,
          });
        }
      }
    );
  }
);

router.get("/checkUserByEmail", (req, res, next) => {
  User.findOne({ email: req.query.email }, "_id", (err, user) => {
    if (err) {
      console.log("err", err);
      res.json({ success: false, message: "Error occurred" });
    } else {
      res.json({ success: true, message: "User found", object: user });
    }
  });
});

router.post("/resetPassword/:id", function (req, res) {
  User.findById(req.params.id, (err, user) => {
    if (err) {
      console.log("err", err);
      res.json({ success: false, message: "Failed to find user" });
    } else if (user) {
      user.password = req.body.password;
      user.save(function (err) {
        if (err) {
          return res.json({ success: false, msg: "Failed to reset password" });
        }
        res.json({ success: true, msg: "Successfully resetted password" });
      });
    } else {
      res.json({ success: false, msg: "User not found" });
    }
  });
});

router.get("/:id", function (req, res) {
  User.findById(req.params.id, (err, user) => {
    if (err) {
      res.json({ success: false, message: "Failed to find user" });
    } else {
      res.json({ success: true, message: "User found", object: user });
    }
  });
});

module.exports = router;
