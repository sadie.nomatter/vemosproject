const express = require("express");
const router = express.Router();
const Uni = require("../models/university");
const passport = require("passport");
require("../passport")(passport);

router.get(
  "/all",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Uni.getAllUniversities((err, universities) => {
      if (err) {
        console.log("error", err);
        res.json({ success: false, message: "Something went wrong..." });
      } else {
        res.json({
          success: true,
          message: "Universities successfully retrieved.",
          universities: universities,
        });
      }
    });
  }
);

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    Uni.createUniversity(req.body.name, (err, university) => {
      if (err) {
        res.json({ success: false, message: "Failed to create Uni" });
      } else {
        res.json({
          success: true,
          message: "Successfully created university",
          object: university,
        });
      }
    });
  }
);

module.exports = router;
