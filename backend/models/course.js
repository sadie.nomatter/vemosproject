const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('../config');

const CourseSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  university: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'University'
  },
  members: [
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
      },
      joinDate: {
        type: Date,
        default: Date.now
      }
    }
  ],
  pals: [
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
      },
      joinDate: {
        type: Date,
        default: Date.now
      }
    }
  ],
  tutors: [
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
      },
      joinDate: {
        type: Date,
        default: Date.now
      },
      rate: {
        type: Number,
        default: 0
      }
    }
  ],
  discussions: [
    {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Discussion'
    }
  ],
  creationDate: {
    type: Date,
    required: true
  }
});

const Course = (module.exports = mongoose.model('Course', CourseSchema));

module.exports.getCourseById = function (id, callback) {
  Course.findById(id, callback).populate('university');
};

module.exports.getCourseByName = function (searchQuery, callback) {
  const query = { name: { $regex: searchQuery, $options: 'i' } };
  Course.find(query, callback).populate('university');
};

module.exports.createCourse = function (name, university, callback) {
  const newCourse = new Course({
    name: name,
    university: university,
    members: [],
    buddies: [],
    tutors: [],
    posts: [],
    creationDate: Date.now()
  });
  newCourse.save(callback);
};

module.exports.findAllmyCoursesByUserId = function (userId, callback) {
  Course.find(
    { $or: [{ 'members._id': userId }, { 'pals._id': userId }, { 'tutors._id': userId }] },
    callback
  ).populate('university');
};

module.exports.findCoursesForMemberByUserId = function (userId, callback) {
  Course.find({ 'members._id': userId }, callback).populate('university');
};

module.exports.findCoursesForPalsByUserId = function (userId, callback) {
  Course.find({ 'pals._id': userId }, callback).populate('university');
};

module.exports.findCoursesForTutorsByUserId = function (userId, callback) {
  Course.find({ 'tutors._id': userId }, callback).populate('university');
};

module.exports.addDiscussion = function (courseId, discussionId, callback) {
  Course.findByIdAndUpdate(courseId, { $push: { discussions: discussionId } }, callback);
};

module.exports.removeDiscussion = function (courseId, discussionId, callback) {
  Course.findByIdAndUpdate(courseId, { $pull: { discussions: discussionId } }, callback);
};

module.exports.findRecentlyJoinedMember = function (userId, callback) {
  Course.find(
    { 'members._id': userId },
    null,
    { limit: 5, sort: { 'members.joinDate': -1 } },
    callback
  ).populate('university');
};

module.exports.findRecentlyJoinedPal = function (userId, callback) {
  Course.find(
    { 'pals._id': userId },
    null,
    { limit: 5, sort: { 'pals.joinDate': -1 } },
    callback
  ).populate('university');
};

module.exports.findRecentlyJoinedTutor = function (userId, callback) {
  Course.find(
    { 'tutors._id': userId },
    null,
    { limit: 5, sort: { 'tutors.joinDate': -1 } },
    callback
  ).populate('university');
};
