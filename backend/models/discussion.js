const mongoose = require('mongoose');
const CommentSchema = require('./comment');

const DiscussionSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  questionText: {
    type: String
  },
  course: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course',
    required: true
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  creationDate: {
    type: Date,
    default: Date.now()
  },
  comments: [CommentSchema],
  commentsTotal: {
    type: Number
  },
  likes: {
    type: Number
  },
  likedBy: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  ]
});

const Discussion = (module.exports = mongoose.model('Discussion', DiscussionSchema));

module.exports.getDiscussionById = function (id, callback) {
  Discussion.aggregate(
    [
      {
        $match: {
          _id: mongoose.Types.ObjectId(id)
        }
      },
      {
        $lookup: {
          from: 'users',
          localField: 'creator',
          foreignField: '_id',
          as: 'creator'
        }
      },
      { $unwind: '$creator' },
      {
        $lookup: {
          from: 'courses',
          localField: 'course',
          foreignField: '_id',
          as: 'course'
        }
      },
      { $unwind: '$course' },
      {
        $unwind: { path: '$comments', preserveNullAndEmptyArrays: true }
      },
      {
        $sort: { 'comments.likes': -1 }
      },
      {
        $group: {
          _id: '$_id',
          title: { $first: '$title' },
          questionText: { $first: '$questionText' },
          course: { $first: '$course' },
          creator: { $first: '$creator' },
          creationDate: { $first: '$creationDate' },
          comments: {
            $push: '$comments'
          },
          commentsTotal: { $first: '$commentsTotal' },
          likes: { $first: '$likes' },
          likedBy: { $first: '$likedBy' }
        }
      }
    ],
    callback
  );
};

module.exports.createDiscussion = function (newDiscussion, callback) {
  newDiscussion.save(callback);
};

module.exports.getDiscussionsByUser = function (creatorId, callback) {
  Discussion.find({ creator: creatorId }, null, { limit: 5, sort: { creationDate: -1 } }, callback)
    .populate('creator')
    .populate('course');
};

module.exports.getDiscussionsByCourse = function (courseId, callback) {
  Discussion.find({ course: courseId }, null, { sort: { creationDate: -1 } }, callback).populate(
    'creator'
  );
};

module.exports.getDiscussionsByUserAndCourse = function (creatorId, courseId, callback) {
  Discussion.find(
    { creator: creatorId, course: courseId },
    null,
    { limit: 5, sort: { creationDate: -1 } },
    callback
  ).populate('creator');
};
