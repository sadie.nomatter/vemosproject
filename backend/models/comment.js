const mongoose = require('mongoose');
//Only a Schema because it is a child of the Discussion Schema and Model
const CommentSchema = mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  creator: {
    type: {
      _id: mongoose.Schema.Types.ObjectId,
      firstName: String,
      lastName: String,
      username: String
    },
    required: true
  },
  creationDate: {
    type: Date,
    default: Date.now()
  },
  likes: {
    type: Number
  },
  likedBy: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  ]
});

module.exports = CommentSchema;
