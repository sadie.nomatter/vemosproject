const mongoose = require('mongoose');

const NotificationSchema = mongoose.Schema({
  toUser: {
    _id: mongoose.Schema.Types.ObjectId,
    type: String,
    ref: 'User'
  },
  fromUser: {
    _id: mongoose.Schema.Types.ObjectId,
    type: String,
    ref: 'User'
  },
  status: {
    type: String,
    enum: ['accepted', 'rejected', 'pending'],
    default: 'pending'
  },
  requestDate: {
    type: Date,
    default: Date.now()
  },
  responseDate: {
    type: Date
  },
  isRead: {
    type: Boolean,
    default: false
  },
  discussionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Discussion'
  },
  course: {
    type: {
      _id: mongoose.Schema.Types.ObjectId,
      name: String
    }
  },
  requestType: {
    type: String,
    enum: ['pal', 'tutor', 'comment', 'response']
  }
});

const Notification = (module.exports = mongoose.model('Notification', NotificationSchema));

module.exports.createNotification = function (newNotification, callback) {
  newNotification.save(callback);
};

module.exports.getNotificationByToUser = function (toUser, callback) {
  Notification.find({ toUser: toUser }, callback)
    .sort({ requestDate: -1, isRead: 1 })
    .populate('fromUser')
    .populate('discussionId');
};

module.exports.getConnections = function (userId, callback) {
  Notification.find(
    {
      $or: [{ fromUser: userId }, { toUser: userId }],
      status: 'accepted',
      requestType: { $nin: ['comment', 'response'] }
    },
    callback
  )
    .populate('fromUser')
    .populate('toUser');
};

module.exports.getMyPals = function (userId, callback) {
  Notification.find(
    {
      $or: [{ fromUser: userId }, { toUser: userId }],
      status: 'accepted',
      requestType: 'pal'
    },
    callback
  )
    .populate('fromUser')
    .populate('toUser');
};

module.exports.getMyTutors = function (userId, callback) {
  Notification.find(
    {
      fromUser: userId,
      status: 'accepted',
      requestType: 'tutor'
    },
    callback
  )
    .populate('fromUser')
    .populate('toUser');
};

module.exports.getMyTutees = function (userId, callback) {
  Notification.find(
    {
      toUser: userId,
      status: 'accepted',
      requestType: 'tutor'
    },
    callback
  )
    .populate('fromUser')
    .populate('toUser');
};

module.exports.getMyPalsForCourse = function (userId, courseId, callback) {
  Notification.find(
    {
      $and: [
        { $or: [{ fromUser: userId }, { toUser: userId }] },
        { $or: [{ status: 'accepted' }, { status: 'pending' }] }
      ],
      'course._id': courseId,
      requestType: 'pal'
    },
    callback
  )
    .populate('fromUser')
    .populate('toUser');
};

module.exports.getMyTutorsForCourse = function (userId, courseId, callback) {
  Notification.find(
    {
      fromUser: userId,
      $or: [{ status: 'accepted' }, { status: 'pending' }],
      'course._id': courseId,
      requestType: 'tutor'
    },
    callback
  )
    .populate('fromUser')
    .populate('toUser');
};

module.exports.getNotificationToUser = function (toUser, callback) {
  Notification.find(
    {
      toUser: toUser,
      $or: [{ status: 'accepted' }, { status: 'pending' }, { status: 'rejected' }],
      $or: [{ requestType: 'tutor' }, { requestType: 'pal' }]
    },
    null,
    { sort: { requestDate: -1, isRead: 1 } },
    callback
  ).populate('fromUser');
};

module.exports.getNotificationFromUser = function (fromUser, callback) {
  Notification.find(
    {
      fromUser: fromUser,
      $or: [{ status: 'accepted' }, { status: 'pending' }, { status: 'rejected' }],
      $or: [{ requestType: 'tutor' }, { requestType: 'pal' }]
    },
    null,
    { sort: { requestDate: -1, isRead: 1 } },
    callback
  ).populate('toUser');
};
